package sjlib.net.p2ptcp;

/**
 * Implementation of the interface allows to output errors or
 * notices through callback methods.
 * 
 * @author Tetraquark
 * 
 */
public interface NoticeListener {
	
	/**
	 * Callback method for output errors.
	 * 
	 * @param errorMessage	string message about some error.
	 */
	public abstract void error(String errorMessage);
	
	/**
	 * Callback method for output notice.
	 * 
	 * @param errorMessage	string message about some notice.
	 */
	public abstract void notice(String noticeMessage);
}