/*
 * json-simple-1.1.1 library is used, from https://code.google.com/p/json-simple/
 * 
 * By Tetraquark: http://tetraquark.ru
 */
package sjlib.net.p2ptcp;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Class that represents a server p2pnetwork node. Object of this class 
 * listens through {@link java.net.ServerSocket ServerSocket} new connection in <i>thread</i>
 * and create local <i>connections</i> which implement 
 * function of transmitting and receiving data in a particular p2pnetwork node.
 * 
 * @see java.lang.Thread
 * @author Tetraquark
 */
public class ConnectsListener extends Thread{
	
	/**
	 * Main ServerSocket, which will listen port.
	 */
	private ServerSocket listenerSocket;
	
	/**
	 * Port on local machine for input connections and data.
	 */
	private int listenPort;
	
	/**
	 * Various states of this listener.
	 */
	private boolean close;
	
	/**
	 * HashMap, which contains <"IP:port", Connect> active pairs. 
	 * 
	 * @see sjlib.net.p2ptcp.ConnectsListener.Connection
	 */
	private Map <String, Connection> connects;
	
	/**
	 * Abstract callback object to output received data.
	 * 
	 * @see sjlib.net.p2ptcp.OutputListener.
	 */
	private OutputListener outputListnener;
	
	/**
	 * Abstract callback object to output errors or notices.
	 * 
	 * @see sjlib.net.p2ptcp.NoticeListener.
	 */
	private NoticeListener noticeListener;
	
	/**
	 * Constructor to create first p2p-network connect listener - the first network node.
	 * 
	 * @param listenPort 		listening port.
	 * @param outputListnener	the callback object to output received data.
	 * @param noticeListener	the callback object to output errors or notices.
	 * @throws IOException		if an I/O error occurs when initialization the ServerSocket.
	 */
	public ConnectsListener(int listenPort, OutputListener outputListnener, NoticeListener noticeListener) throws IOException{
		this.listenPort = listenPort;
		this.listenerSocket = new ServerSocket(listenPort);
		this.connects = new HashMap<String, Connection>();
		
		this.outputListnener = outputListnener;
		this.noticeListener = noticeListener;
		
		this.close = false;
	}
	
	/**
	 * Constructor to create listener and connect to some node.
	 * 
	 * @param firstAddr 		IPv4 remote node address.
	 * @param remotePort		port of remote node.
	 * @param listenPort		listening port.
	 * @param outputListnener	the callback object to output received data.
	 * @param noticeListener	the callback object to output errors or notices.
	 * @throws IOException		if an I/O error occurs when initialization the ServerSocket.
	 */
	public ConnectsListener(String firstAddr, int remotePort, int listenPort, OutputListener outputListnener, NoticeListener noticeListener) throws IOException{
		this.listenPort = listenPort;
		this.listenerSocket = new ServerSocket(listenPort);
		this.connects = new HashMap<String, Connection>();
		
		this.outputListnener = outputListnener;
		this.noticeListener = noticeListener;
		
		this.close = false;
		addNewConnect(firstAddr, remotePort, true);
	}
	
	/**
	 * Runs the threads, which listen and accept new connections.
	 */
	@Override
	public void run(){
		try{
			while(!close){
				Socket newSocket = null;
				
				newSocket = listenerSocket.accept();
				
				if(newSocket != null){
					
						Connection newConnect = new Connection();
						newConnect.initConnection(newSocket);
						String key = newConnect.getKeySocket();
						if(key != null){
							if(!connects.containsKey(key) && 
									!key.equals(InetAddress.getLocalHost().getHostAddress() + ":" + listenPort) &&
									//for loopback address
									!key.equals(InetAddress.getLoopbackAddress().getHostAddress() + ":" + listenPort)){
								newConnect.start();	
								connects.put(key, newConnect);
								noticeListener.notice("run: " + key);
								if(connects.size() > 1){
									if(newConnect.isNewNode())
										sendConnect(newConnect);
								}
							}
						}
				}
			}	
		} 
		catch(IOException e){
			noticeListener.error("ConnectsListener, run(): " + e.toString());
		}
		finally{
			endConnects();
			try {
				if(this.listenerSocket != null){
					this.listenerSocket.close();
					this.listenerSocket = null;
				}
			} catch (IOException e) {
				noticeListener.error("ConnectsListener, run(), finally{}: " + e.toString());
			}
		}
	}
	
	/**
	 * Send data to <b>all</b> active connections.
	 * 
	 * @see org.json.simple.JSONObject
	 * 
	 * @param jsonData		some data in JSON format.
	 * @throws IOException	if an Output error occurs when the Connection object send data.
	 */
	@SuppressWarnings("unchecked")
	public void sendDataToAll(JSONObject jsonData) throws IOException{
		jsonData.put("service", "send_jsondata");
		for (String key : connects.keySet()){
			Connection receiverConn = connects.get(key);
			receiverConn.sendData(jsonData);
		}
	}
	
	/**
	 * Send data by the key.
	 * @param key connection identifier. (key == "IP:port")
	 */
	@SuppressWarnings("unchecked")
	public void sendDataTo(String key, JSONObject jsonData) throws IOException{
		jsonData.put("service", "send_jsondata");
		Connection receiverConn = connects.get(key);
		receiverConn.sendData(jsonData);
	}
	
	/**
	 * Stops the listener.
	 */
	public void stopListener(){
		this.close = true;
	}
	
	/**
	 * Creates {@link java.util.ArrayList ArrayList}, which contains {@link java.lang.String Strings} of sockets 
	 * like "Address:port" of nodes.
	 * 
	 * @return	container which contains "Listen address:remotePort" strings.
	 */
	public ArrayList<String> getConnList(){
		ArrayList<String> connectsList = new ArrayList<>();
		for (String key : connects.keySet()){
			Connection sendingConn = connects.get(key);
			connectsList.add(sendingConn.getListenAddr() + ":" + sendingConn.getRemotePort());
		}
		return connectsList;
	}
	
	/**
	 * Sends data to all active connections about new connection, which the listener accepted.
	 * 
	 * @param sendingConn	Connection, which was accepted.
	 * @throws IOException	if an Output error occurs when the Connection object send data.
	 * @see sjlib.net.p2ptcp.ConnectsListener.Connection
	 */
	@SuppressWarnings("unchecked")
	private void sendConnect(Connection sendingConn) throws IOException{
		JSONObject jsonSendingConn = new JSONObject();
		jsonSendingConn.put("service", "send_connect");
		jsonSendingConn.put("address", sendingConn.getListenAddr());
		jsonSendingConn.put("port", sendingConn.getRemotePort());	
		JSONObject jsonReceiver;
		for(String key : connects.keySet()){
			Connection receiverConn = connects.get(key);
			jsonReceiver = new JSONObject();
			jsonReceiver.put("service", "send_connect");
			jsonReceiver.put("address", receiverConn.getListenAddr());
			jsonReceiver.put("port", receiverConn.getRemotePort());
			receiverConn.sendData(jsonSendingConn);
		}
	}
	
	/**
	 * Sends service message to all active connections about shutdown the listener.
	 */
	@SuppressWarnings("unchecked")
	private void endConnects(){
		JSONObject jsonSendingData = new JSONObject();
		jsonSendingData.put("service", "end_connect");
		synchronized (connects){
			for(String key : connects.keySet()){
				Connection receiverConn = connects.get(key);
				try {
					receiverConn.sendData(jsonSendingData);
					receiverConn.stopConnect();	
				} catch (IOException e) {
					noticeListener.error("ConnectsListener, endConnect(): " + e.toString());
				}
			}
		}
	}
	
	/**
	 * Create new {@link sjlib.net.p2ptcp.ConnectsListener.Connection Connection} and add it to <i>connects</i> HashMap.
	 * 
	 * @param connectAddr	IPv4 address of new connection.
	 * @param connectPort	port of new connection.
	 * @param isNewNetNode  TODO
	 * @see sjlib.net.p2ptcp.ConnectsListener#connects
	 */
	private void addNewConnect(String connectAddr, int connectPort, boolean isNewNetNode){
		try {
			String NewConnectSocket = connectAddr + ":" + connectPort;
			if(!connects.containsKey(NewConnectSocket) 
					&& !NewConnectSocket.equals(InetAddress.getLocalHost().getHostAddress() + ":" + listenPort)
					//for loopback address
					&& !NewConnectSocket.equals(InetAddress.getLoopbackAddress().getHostAddress() + ":" + listenPort)){
				Connection newConnect = new Connection();
				newConnect.initConnection(connectAddr, connectPort, isNewNetNode);
				String key = newConnect.getKeySocket();
				if(key != null){
					this.connects.put(NewConnectSocket, newConnect);
					noticeListener.notice("addNewConn: " + NewConnectSocket);
					newConnect.start();
				}
			}
		} catch (IOException e) {
			noticeListener.error("ConnectsListener, addNewConnect(): " + e.toString());
		}
	}
	
	/**
	 * Remove {@link sjlib.net.p2ptcp.ConnectsListener.Connection Connection} object from <i>connects</i> HashMap.
	 * 
	 * @param key	IPv4 address of connection node.
	 * @see sjlib.net.p2ptcp.ConnectsListener#connects
	 */
	private void removeConnect(String key){
		synchronized (connects) {
			this.connects.remove(key);
		}
	}
	
	/**
	 * Class <b>Connection</b> objects which send, receive and process data from a specific socket.
	 */
	private final class Connection extends Thread{
		
		/**
		 * The main socket through which are exchanged with some node of network.
		 */
		private Socket connectSocket;
		
		/**
		 * Remote node address.
		 */
		private String listenAddress;
		
		/**
		 * Remote node input port.
		 */
		private int remotePort = -1;
		
		private ObjectInputStream inputStream;
		private ObjectOutputStream outputStream;
		
		/**
		 * Boolean flag to stop connection.
		 */
		private boolean stopListen;
		
		private int isNewNode = -1;
		
		/**
		 * Initial method that is called when {@link sjlib.net.p2ptcp.ConnectsListener ConnectsListener} accepts some connection.
		 * 
		 * @param socket		Socket from ConnectsListener.
		 * @throws IOException	if an I/O error occurs when initialization the I/O streams.
		 * @see java.net.Socket
		 */
		public void initConnection(Socket socket) throws IOException{
			this.listenAddress = socket.getInetAddress().getHostAddress();
			
			this.connectSocket = socket;
			this.outputStream = new ObjectOutputStream(socket.getOutputStream());
			this.inputStream = new ObjectInputStream(socket.getInputStream());
			
			this.stopListen = false;
			
			JSONParser jsParser = new JSONParser();
			JSONObject jsObj;
			String inputData = inputStream.readUTF();
			try{
				jsObj = (JSONObject) jsParser.parse(inputData);
				isNewNode = (int) ((long) jsObj.get("nodeflag"));
				String port = (String)jsObj.get("port");
				this.remotePort = Integer.parseInt(port);
			}
			catch(ParseException e){
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/**
		 * Initial method that is called when {@link sjlib.net.p2ptcp.ConnectsListener ConnectsListener} tries connect to some node.
		 * 
		 * @param listenAddress		address of network node.
		 * @param remotePort		remote node input port.
		 * @param isNewNetNode 		TODO
		 * @throws IOException		if an I/O error occurs when initialization the I/O streams.
		 */
		public void initConnection(String listenAddress, int remotePort, boolean isNewNetNode) throws IOException{
			this.listenAddress = listenAddress;
			this.remotePort = remotePort;
			
			this.connectSocket = new Socket(listenAddress, remotePort);
			this.outputStream = new ObjectOutputStream(connectSocket.getOutputStream());
			this.inputStream = new ObjectInputStream(connectSocket.getInputStream());
			
			this.stopListen = false;
			
			JSONObject jsonSendingConn = new JSONObject();
			jsonSendingConn.put("service", "input_connect");
			jsonSendingConn.put("port", ConnectsListener.this.listenPort + "");	
			
			if(isNewNetNode)
				jsonSendingConn.put("nodeflag", 1);
			else
				jsonSendingConn.put("nodeflag", 0);
			
			outputStream.writeUTF(jsonSendingConn.toJSONString());
			outputStream.flush();
		}
		
		@Override
		public void run(){
			String inputData = null;
			JSONObject jsObj;
			JSONParser jsParser;
			
			try{
				while(!stopListen){
					jsObj = new JSONObject();
					jsParser = new JSONParser();
					
					inputData = inputStream.readUTF();
					
					try{
						jsObj = (JSONObject) jsParser.parse(inputData);
					} catch(ParseException e){
						ConnectsListener.this.noticeListener.error("Parse exception: " + e.toString());
						continue;
					}
					
					String serviceMsg = (String) jsObj.get("service");
					switch(serviceMsg){
						case "send_connect":{
							long port = (long) jsObj.get("port");
							String adr = (String) jsObj.get("address");
							addNewConnect(adr, (int) port, false);
							break;
						}
						case "send_jsondata":{
							jsObj.remove("service");
							jsObj.put("send_from", getListenAddr() + ":" + remotePort);
							outputListnener.dataOutput(jsObj);
							break;
						}
						case "end_connect":{
							stopConnect();
						}
					}
				}	 
			} catch (IOException e) {
				stopConnect();
			}
			finally{
				try {
					this.connectSocket.close();
					this.connectSocket = null;
					this.inputStream = null;
					this.outputStream = null;
					this.stopListen = true;
					ConnectsListener.this.noticeListener.notice("Stop connect from: " + getListenAddr());
				} catch (IOException e) {
					ConnectsListener.this.noticeListener.error("Connect, run(), {}finally: " + e.toString());
				}
				removeConnect(this.listenAddress + ":" + getRemotePort());
			}
		}
		
		/**
		 * Send JSON object with data throw object output stream.
		 * 
		 * @param jsData		some JSON data in JSONObject
		 * @throws IOException	if an Output error occurs
		 * @see org.json.simple.JSONObject
		 */
		public void sendData(JSONObject jsData) throws IOException{
			outputStream.writeUTF(jsData.toString());
			outputStream.flush();
		}
		
		/**
		 * Stops connection.
		 */
		public void stopConnect(){
			this.stopListen = true;
		}
		
		public String getListenAddr(){
			return this.listenAddress;
		}
		
		/**
		 * Returns port which listens remote node.
		 * 
		 * @return remote node port
		 * @see sjlib.net.p2ptcp.ConnectsListener#listenPort
		 */
		public int getRemotePort(){
			return this.remotePort;
		}
		
		public String getKeySocket(){
			if(remotePort != -1)
				return this.listenAddress + ":" + this.remotePort;
			return null;
		}
		
		public boolean isNewNode(){
			if(isNewNode == 1)
				return true;
			return false;
		}
		
	}
}
