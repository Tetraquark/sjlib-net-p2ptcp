package sjlib.net.p2ptcp;

import org.json.simple.JSONObject;

/**
 * Implementation of the interface allows to output JSON data
 * through callback method.
 * 
 * @author Tetraquark
 */
public interface OutputListener {
	
	/**
	 * Callback method for output data.
	 * 
	 * @param jsonData	contains data.
	 */
	public abstract void dataOutput(JSONObject jsonData);
}
