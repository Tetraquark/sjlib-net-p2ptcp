# README

Simple Java Library - the common name of a small collection of simple libraries for Java. These libraries are for educational practice, to learn the basics of the Java language. Therefore, the authors of this libraries do not claim absolute correctness of the implementation.

## Net - P2pTCP

P2pTCP - simple Java library is part of the package *net* (network). The library implements the work peer-to-peer network based on TCP protocol. The library is based on java.net package in particularly used java.net.ServerSocket and java.net.Socket classes.

*The current version work only in LAN*

##### External dependencies

P2pTCP uses JSON simple library (json-simple-1.1.1). So, you need to put the json-simple-1.1.1.jar (or compatible version) and sjlib-net-p2ptcp.jar in your CLASSPATH before compiling and running the code.

[JSON-simple project link](https://code.google.com/p/json-simple/)

## How to use 

First of all, you have to create classes for output data which implements two interfaces: **OutputListener** and **NoticeListener**.

**OutputListener** has one abstract method **void dataOutput(JSONObject jsonData)** for callback. *jsonData* object has input data from network nodes. In this object has key "*send_from*" which contains sender IPv4 address.

**NoticeListener** has two abstract methods **void error(String errorMessage)** and **void notice(String noticeMessage)** for callback. This method for outputting errors and information about the application (for example, create new Connection). 

The core of the network application should be **ConnectsListener** class. 

**ConnectsListener** has 2 constructors:
ConnectsListener(int listenPort, OutputListener outputListnener, NoticeListener noticeListener) throws IOException;
ConnectsListener(String firstAddr, int listenPort, OutputListener outputListnener, NoticeListener noticeListener) throws IOException;

If you create first network node, you have to use first constructor ( ConnectsListener(int, OutputListener, NoticeListener) ).
If you create not first network node, you have to use second constructor( ConnectsListener(String, int, OutputListener, NoticeListener) ).

##Example of use
Creating first network node:
```
#!Java
public class p2pChat implements OutputListener, NoticeListener{
		
	private ConnectsListener connectsListener;
	private int port = 10888;

	/**
	* p2pChat class constructor.
	* 
	* @param firstOrNot	= 0 (first node of network); = 1 (not first);
	* @param nodeIP		IPv4 address of some network node.
	*/
	public p2pChat(int firstOrNot, String nodeIP, int remotePort){
		try {
			if(firstOrNot == 0)
				startFirst();
			else
				startNotFirst(nodeIP, remotePort);
			
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public void startFirst() throws IOException{
		connectsListener = new ConnectsListener(this.port, this, this);
		connectsListener.start();
	}
	
	public void startNotFirst(String nodeIP, int remotePort) throws IOException{
		connectsListener = new ConnectsListener(nodeIP, remotePort, this.port, this, this);
		connectsListener.start();
	}
		
	public void sendData(String someData) throws IOException{
		JSONObject jsonData = new JSONObject();
		jsonData.put("somedata", someData);			//call key data as you wish
		connectsListener.sendDataToAll(jsonData); 	//send data to all active connects
	}
	
	public void stopChat(){
		connectsListener.stopListener();
	}
	
	@Override
	public void error(String errorMessage) {
		System.err.println(errorMessage);
	}
	
	@Override
	public void notice(String noticeMessage) {
		System.out.println(noticeMessage);
	}
	@Override
	
	public void dataOutput(JSONObject jsonData) {
		// "send_from" contains sender IP address and sender port string
		// like: "192.168.0.1:5050"
		System.out.println(jsonData.get("send_from").toString() + ": " + jsonData.get("somedata").toString());
	}
}
```